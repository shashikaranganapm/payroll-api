package lk.ijse.payroll.repository;

import lk.ijse.payroll.entity.Deduction;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DeductionRepository extends JpaRepository<Deduction, Long> {
}
