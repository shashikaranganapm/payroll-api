package lk.ijse.payroll.repository;

import lk.ijse.payroll.entity.Types;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TypesRepository extends JpaRepository<Types, String> {
}
