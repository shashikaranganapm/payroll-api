package lk.ijse.payroll.repository;

import lk.ijse.payroll.entity.Addition;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AdditionRepository extends JpaRepository<Addition, Long> {
}
