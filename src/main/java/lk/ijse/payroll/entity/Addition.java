package lk.ijse.payroll.entity;

import javax.persistence.*;

@Entity
public class Addition {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long additionId;
    @ManyToOne
    private Employee employee;
    private String  types;
    private String date;
    private double totalAmount;

    public Addition(Long additionId, Long id, String typeId, String date, double totalAmount) {
    }

    public Addition(Employee employee, String types, String date, double totalAmount) {
        this.employee = employee;
        this.types = types;
        this.date = date;
        this.totalAmount = totalAmount;
    }

    public Long getAdditionId() {
        return additionId;
    }

    public void setAdditionId(Long additionId) {
        this.additionId = additionId;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public String getTypes() {
        return types;
    }

    public void setTypes(String types) {
        this.types = types;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(double totalAmount) {
        this.totalAmount = totalAmount;
    }

    @Override
    public String toString() {
        return "Addition{" +
                "additionId=" + additionId +
                ", employee=" + employee +
                ", types='" + types + '\'' +
                ", date='" + date + '\'' +
                ", totalAmount=" + totalAmount +
                '}';
    }
}
