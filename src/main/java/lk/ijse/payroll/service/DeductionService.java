package lk.ijse.payroll.service;

import lk.ijse.payroll.dto.DeductionDTO;
import lk.ijse.payroll.entity.Deduction;

import java.util.ArrayList;

public interface DeductionService {

    boolean saveDeduction(DeductionDTO deductionDTO);

    ArrayList<DeductionDTO> findAllDeduction();

    Deduction findDeductionById(Long deductionId);

    boolean deleteDeduction(Long deductionId);
}
