package lk.ijse.payroll.service;

import lk.ijse.payroll.dto.AdditionDTO;
import lk.ijse.payroll.entity.Addition;

import java.util.ArrayList;

public interface AdditionService {

     Boolean saveAddition(Addition addition);

     ArrayList<AdditionDTO> findAllAddition();

     Addition findAdditionById(Long additionId);

     boolean deleteAddition(Long additionId);

}
