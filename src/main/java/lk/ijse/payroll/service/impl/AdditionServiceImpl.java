package lk.ijse.payroll.service.impl;

import lk.ijse.payroll.dto.AdditionDTO;
import lk.ijse.payroll.entity.Addition;
import lk.ijse.payroll.entity.Employee;
import lk.ijse.payroll.entity.Types;
import lk.ijse.payroll.repository.AdditionRepository;
import lk.ijse.payroll.service.AdditionService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;

@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class AdditionServiceImpl implements AdditionService {

    @Autowired
    private AdditionRepository additionRepository;

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public Boolean saveAddition(Addition addition) {
        try {
//            Employee employee = new Employee();
//            employee.setId(addition.getEmployee().getId());
//            addition.setAdditionId(addition.getAdditionId());
//            addition.setEmployee(employee);
//            addition.setTypes(addition.getTypes());
//            addition.setDate(addition.getDate());
//            addition.setTotalAmount(addition.getTotalAmount());
//            BeanUtils.copyProperties(addition, addition);
//            additionRepository.save(addition);
//            return true;
            Addition addition1 = new Addition(
                addition.getAdditionId(),
                addition.getEmployee().getId(),
                addition.getTypes(),
                addition.getDate(),
                addition.getTotalAmount());
        additionRepository.save(addition1);
        return true;
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return false;
//        Addition addition1 = new Addition(
//                addition.getAdditionId(),
//                addition.getEmployee().getId(),
//                addition.getTypes(),
//                addition.getDate(),
//                addition.getTotalAmount());
//        additionRepository.save(addition1);
//        return true;
    }

//    @Override
//    @Transactional(propagation = Propagation.REQUIRED)
//    public boolean saveEmployee(EmployeeDTO employeeDTO) {
//        Employee employee = new Employee(
//                employeeDTO.getId(),
//                employeeDTO.getName(),
//                employeeDTO.getDesignation(),
//                employeeDTO.getSalary(),
//                employeeDTO.getAddress(),
//                employeeDTO.getContactNo(),
//                employeeDTO.getJoinDate(),
//                employeeDTO.getDob(),
//                employeeDTO.getGender());
//        employeeRepository.save(employee);
//        return true;
//    }

    @Override
    public ArrayList<AdditionDTO> findAllAddition() {
        return null;
    }

    @Override
    public Addition findAdditionById(Long additionId) {
        return null;
    }

    @Override
    public boolean deleteAddition(Long additionId) {
        return false;
    }
}
