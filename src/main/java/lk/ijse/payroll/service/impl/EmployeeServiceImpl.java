package lk.ijse.payroll.service.impl;

import lk.ijse.payroll.dto.EmployeeDTO;
import lk.ijse.payroll.entity.Employee;
import lk.ijse.payroll.exception.RecordNotFoundException;
import lk.ijse.payroll.repository.EmployeeRepository;
import lk.ijse.payroll.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class EmployeeServiceImpl implements EmployeeService {
    @Autowired
    private EmployeeRepository employeeRepository;

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public boolean saveEmployee(EmployeeDTO employeeDTO) {
        Employee employee = new Employee(
                employeeDTO.getId(),
                employeeDTO.getName(),
                employeeDTO.getDesignation(),
                employeeDTO.getSalary(),
                employeeDTO.getAddress(),
                employeeDTO.getContactNo(),
                employeeDTO.getJoinDate(),
                employeeDTO.getDob(),
                employeeDTO.getGender());
        employeeRepository.save(employee);
        return true;
    }

    @Override
    public EmployeeDTO updateEmployee(EmployeeDTO employeeDTO) {
//        Employee employee = findEmployeeById(employeeDTO.getId());
//        employee.setName(employeeDTO.getName());
//        employee.setDesignation(employeeDTO.getDesignation());
//        employee.setSalary(employeeDTO.getSalary());
//        employee.setStatus(employeeDTO.getStatus());
//        employee.setAddress(employeeDTO.getAddress());
//        employee.setContactNo(employeeDTO.getContactNo());
//        employee.setJoinDate(employeeDTO.getJoinDate());
//        employee.setDob(employeeDTO.getDob());
//        employee.setGender(employeeDTO.getGender());
//        return new EmployeeDTO(employeeRepository.save(employee));
        Optional<Employee> findEmployee=employeeRepository.findById(employeeDTO.getId());
        if (findEmployee.isPresent()){
            Employee employee=findEmployee.get();
            employee.setName(employeeDTO.getName());
            employee.setDesignation(employeeDTO.getDesignation());
            employee.setSalary(employeeDTO.getSalary());
            employee.setAddress(employeeDTO.getAddress());
            employee.setContactNo(employeeDTO.getContactNo());
            employee.setJoinDate(employeeDTO.getJoinDate());
            employee.setDob(employeeDTO.getDob());
            employee.setGender(employeeDTO.getGender());
            employee=employeeRepository.save(employee);
            return new EmployeeDTO(employee);
        }else {
            throw new RecordNotFoundException(String.format("not found :%s", employeeDTO.getId()));
        }
    }

    @Override
    public ArrayList<EmployeeDTO> findAllEmployees() {
        List<Employee> employees = employeeRepository.findAll();
        ArrayList<EmployeeDTO> employeeDTOS = new ArrayList<>();
        for (Employee employee : employees) {
            EmployeeDTO employeeDTO = new EmployeeDTO(
                    employee.getId(),
                    employee.getName(),
                    employee.getDesignation(),
                    employee.getSalary(),
                    employee.getAddress(),
                    employee.getContactNo(),
                    employee.getJoinDate(),
                    employee.getDob(),
                    employee.getGender());
            employeeDTOS.add(employeeDTO);
        }
        return employeeDTOS;
    }

    @Override
    public Employee findEmployeeById(Long employeeId) {
//        return Optional.of(employeeRepository.findByEmployeeId(employeeId))
//                .filter(Optional::isPresent)
//                .map(Optional::get)
//                .orElseThrow(() -> new RecordNotFoundException(String.format("Employee not found %s", employeeId)));
        Employee employee=null;
        try {
            employee =employeeRepository.findByEmployeeId(employeeId);
        }catch (Exception e){

        }
        return employee;
//        Employee employee = employeeRepository.findById(employeeId).get();
//        EmployeeDTO employeeDTO = new EmployeeDTO(
//                employee.getId(),
//                employee.getName(),
//                employee.getDesignation(),
//                employee.getSalary(),
//                employee.getStatus(),
//                employee.getAddress(),
//                employee.getContactNo(),
//                employee.getJoinDate(),
//                employee.getDob(),
//                employee.getGender()
//        );
//        return employeeDTO;
    }

    @Override
    public boolean deleteEmployee(Long employeeId) {
        employeeRepository.deleteById(employeeId);
        return true;
    }
}
