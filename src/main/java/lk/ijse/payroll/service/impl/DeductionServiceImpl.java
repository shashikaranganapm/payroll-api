package lk.ijse.payroll.service.impl;

import lk.ijse.payroll.dto.DeductionDTO;
import lk.ijse.payroll.entity.Deduction;
import lk.ijse.payroll.repository.DeductionRepository;
import lk.ijse.payroll.service.DeductionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;

@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class DeductionServiceImpl implements DeductionService {

    @Autowired
    private DeductionRepository deductionRepository;

    @Override
    public boolean saveDeduction(DeductionDTO deductionDTO) {
        try {
//            Employee employee = new Employee();
//            employee.setId(deductionDTO.getEmployee().getId());
//            deductionDTO.setDeductionId(deductionDTO.getDeductionId());
//            deductionDTO.setEmployee(employee);
//            deductionDTO.setTypes(addition.getTypes());
//            deductionDTO.setDate(addition.getDate());
//            deductionDTO.setTotalAmount(addition.getTotalAmount());
//            BeanUtils.copyProperties(addition, addition);
//            additionRepository.save(addition);
//            return true;
            Deduction deduction = new Deduction(
                    deductionDTO.getDeductionId(),
                    deductionDTO.getEmployee().getId(),
                    deductionDTO.getTypes(),
                    deductionDTO.getDate(),
                    deductionDTO.getTotalAmount());
            deductionRepository.save(deduction);
            return true;
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return false;
    }

    @Override
    public ArrayList<DeductionDTO> findAllDeduction() {
        return null;
    }

    @Override
    public Deduction findDeductionById(Long deductionId) {
        return null;
    }

    @Override
    public boolean deleteDeduction(Long deductionId) {
        return false;
    }
}
