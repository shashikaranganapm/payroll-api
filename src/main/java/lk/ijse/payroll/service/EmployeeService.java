package lk.ijse.payroll.service;

import lk.ijse.payroll.dto.EmployeeDTO;
import lk.ijse.payroll.entity.Employee;

import java.util.ArrayList;

public interface EmployeeService {

    boolean saveEmployee(EmployeeDTO employeeDTO);

    EmployeeDTO updateEmployee(EmployeeDTO employeeDTO);

    ArrayList<EmployeeDTO> findAllEmployees();

    Employee findEmployeeById(Long employeeId);

    boolean deleteEmployee(Long employeeId);

}
