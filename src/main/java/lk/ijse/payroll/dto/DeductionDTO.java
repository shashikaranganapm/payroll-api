package lk.ijse.payroll.dto;

import lk.ijse.payroll.entity.Employee;
import lk.ijse.payroll.entity.Types;

public class DeductionDTO {
    private Long deductionId;
    private Employee employee;
    private String types;
    private String date;
    private double totalAmount;

    public DeductionDTO() {
    }

    public DeductionDTO(Long deductionId, Employee employee, String types, String date, double totalAmount) {
        this.deductionId = deductionId;
        this.employee = employee;
        this.types = types;
        this.date = date;
        this.totalAmount = totalAmount;
    }

    public Long getDeductionId() {
        return deductionId;
    }

    public void setDeductionId(Long deductionId) {
        this.deductionId = deductionId;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public String getTypes() {
        return types;
    }

    public void setTypes(String types) {
        this.types = types;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(double totalAmount) {
        this.totalAmount = totalAmount;
    }

    @Override
    public String toString() {
        return "DeductionDTO{" +
                "deductionId=" + deductionId +
                ", employee=" + employee +
                ", types='" + types + '\'' +
                ", date='" + date + '\'' +
                ", totalAmount=" + totalAmount +
                '}';
    }
}
