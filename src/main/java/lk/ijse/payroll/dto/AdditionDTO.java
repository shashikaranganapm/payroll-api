package lk.ijse.payroll.dto;

import lk.ijse.payroll.entity.Employee;
import lk.ijse.payroll.entity.Types;

public class AdditionDTO {
    private Long additionId;
    private Employee employee;
    private String types;
    private String date;
    private double totalAmount;

    public AdditionDTO() {
    }

    public AdditionDTO(Long additionId, Employee employee, String types, String date, double totalAmount) {
        this.additionId = additionId;
        this.employee = employee;
        this.types = types;
        this.date = date;
        this.totalAmount = totalAmount;
    }

    public Long getAdditionId() {
        return additionId;
    }

    public void setAdditionId(Long additionId) {
        this.additionId = additionId;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public String getTypes() {
        return types;
    }

    public void setTypes(String types) {
        this.types = types;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(double totalAmount) {
        this.totalAmount = totalAmount;
    }

    @Override
    public String toString() {
        return "AdditionDTO{" +
                "additionId=" + additionId +
                ", employee=" + employee +
                ", types='" + types + '\'' +
                ", date='" + date + '\'' +
                ", totalAmount=" + totalAmount +
                '}';
    }
}
