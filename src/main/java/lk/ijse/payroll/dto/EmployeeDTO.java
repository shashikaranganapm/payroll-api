package lk.ijse.payroll.dto;

import lk.ijse.payroll.entity.Employee;

public class EmployeeDTO {
    private Long employeeId;
    private String name;
    private String designation;
    private double salary;
    private String address;
    private int contactNo;
    private String joinDate;
    private String dob;
    private String gender;

    public EmployeeDTO() {
    }

    public EmployeeDTO(Long employeeId, String name, String designation, double salary,String address, int contactNo, String joinDate, String dob, String gender) {
        this.employeeId = employeeId;
        this.name = name;
        this.designation = designation;
        this.salary = salary;
        this.address = address;
        this.contactNo = contactNo;
        this.joinDate = joinDate;
        this.dob = dob;
        this.gender = gender;
    }

    public EmployeeDTO(Employee employee) {

    }

    public Long getId() {
        return employeeId;
    }

    public void setId(Long employeeId) {
        this.employeeId = employeeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getContactNo() {
        return contactNo;
    }

    public void setContactNo(int contactNo) {
        this.contactNo = contactNo;
    }

    public String getJoinDate() {
        return joinDate;
    }

    public void setJoinDate(String joinDate) {
        this.joinDate = joinDate;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }


}
