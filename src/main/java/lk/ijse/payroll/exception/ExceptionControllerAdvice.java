package lk.ijse.payroll.exception;

//import com.misyn.credentialmanager.service.dto.CustomExceptionDto;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.Collection;
import java.util.stream.Collectors;

@ControllerAdvice
public class ExceptionControllerAdvice {

//    @ExceptionHandler
//    public ResponseEntity<CustomExceptionDto> handleRecordNotFoundExceptions(RecordNotFoundException ex) {
//        CustomExceptionDto custom = new CustomExceptionDto();
//        custom.setMessage(ex.getMessage());
//        return new ResponseEntity<>(custom, HttpStatus.NOT_FOUND);
//    }
//
//    @ExceptionHandler
//    public ResponseEntity<CustomExceptionDto> handleIllegalArgumentException(IllegalArgumentException ex) {
//        return new ResponseEntity<>(new CustomExceptionDto("Client operation failed "), HttpStatus.BAD_REQUEST);
//    }
//
//    @ExceptionHandler
//    public ResponseEntity<Collection<CustomExceptionDto>> handleMethodArgumentExceptions(MethodArgumentNotValidException ex) {
//        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
//                ex.getBindingResult()
//                        .getAllErrors().stream().map(
//                        error -> new CustomExceptionDto(error.getDefaultMessage()))
//                        .collect(Collectors.toList()
//                        )
//        );
//    }
//
//    @ExceptionHandler
//    public ResponseEntity<CustomExceptionDto> handleServerExceptions(Exception ex) {
//        return new ResponseEntity<>(
//                new CustomExceptionDto("Server operation failed!"), HttpStatus.INTERNAL_SERVER_ERROR
//        );
//    }
//
//    @ExceptionHandler
//    public ResponseEntity<CustomExceptionDto> handleInvalidArgumentExceptions(InvalidArgumentException ex) {
//        CustomExceptionDto custom = new CustomExceptionDto();
//        custom.setMessage(ex.getMessage());
//        return new ResponseEntity<>(custom, HttpStatus.BAD_REQUEST);
//    }
//
//    @ExceptionHandler
//    public ResponseEntity<CustomExceptionDto> handleMissingServletRequestParameterException(MissingServletRequestParameterException ex) {
//        CustomExceptionDto custom = new CustomExceptionDto();
//        custom.setMessage(ex.getMessage());
//        return new ResponseEntity<>(custom, HttpStatus.BAD_REQUEST);
//    }
}
