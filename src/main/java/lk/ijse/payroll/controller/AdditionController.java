package lk.ijse.payroll.controller;

import lk.ijse.payroll.entity.Addition;
import lk.ijse.payroll.service.AdditionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequestMapping(value = "api/v1/addition")
public class AdditionController {

    @Autowired
    AdditionService additionService;

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public Boolean saveAddition(@RequestBody Addition addition) {
        return additionService.saveAddition(addition);
    }
}
